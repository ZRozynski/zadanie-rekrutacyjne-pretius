# Zadanie rekrutacyjne Pretius -03.10.2018

## Jak uruchomić
- Dla IntelliJ IDEA bądź Eclipse
    - Wystarczy zaimportować projekt, przejść do klasy Main.java i uruchomić. 
    
- Z poziomu wiersza poleceń:
    - Przechodzimy do katalogu, w którym znajduje się projekt, otwieramy Konsolę i:
        - Budujemy .jar używając "mvn clean package"
        - Uruchamiamy aplikację komendą "mvn exec:java"

- Z programu wychodzimy wpisaując w wiersz poleceń pożądane polecenie. ("stop")

## Narzędzia, za pomocą których stworzono projekt 
- System operacyjny: Windows 10 Education
- IDE: IntelliJ IDEA Ultimate 2018.1
- JDK: 
	- java version "1.8.0_171"
	- Java(TM) SE Runtime Environment (build 1.8.0_171-b11)
	- Java HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)
- MAVEN: Apache Maven 3.3.9 (bb52d8502b132ec0a5a3f4c09453c07478323dc5; 2015-11-10T17:41:47+01:00)
