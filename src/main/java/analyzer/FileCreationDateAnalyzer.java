package analyzer;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

public class FileCreationDateAnalyzer {

    private static int getFileCreationHour(File file) {
        long lastModifiedDate = file.lastModified();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(lastModifiedDate));
        return calendar.get(Calendar.HOUR);
    }

    public static boolean isFileCreationDateEven(File file) {
        return getFileCreationHour(file) % 2 == 0;
    }
}
