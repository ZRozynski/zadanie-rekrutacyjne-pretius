package thread;

import java.util.Scanner;

public class AppStopRunnable implements Runnable {

    public boolean userDecidedToStop = false;

    @Override
    public void run() {
        while (!userDecidedToStop){
            Scanner scanner = new Scanner(System.in);
            System.out.print("An infinite while() loop is running :( -- enter 'stop' to exit: ");
            String input = scanner.nextLine();
            if(input.equals("stop")){
                userDecidedToStop = true;
                break;
            }
        }
    }
}
