import analyzer.FileCreationDateAnalyzer;
import creator.ResourcesFileCreator;
import log.FileCountsLogger;
import thread.AppStopRunnable;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Runner {

    File homeDirectory;
    Runnable appStopRunnable = new AppStopRunnable();

    public void run() throws Exception {
        initializeDirectiories();
        Thread appStopThread = new Thread(appStopRunnable);
        appStopThread.start();

        while (true) {
            if(((AppStopRunnable)appStopRunnable).userDecidedToStop)
                break;

            File[] files = homeDirectory.listFiles();
            for (File file : files) {
                if (!file.isFile() || file.getName().endsWith(".txt"))
                    continue;
                else {
                    FileCountsLogger.processedFiles++;
                    String fileName = file.getName();
                    if (fileName.endsWith(".xml")) {
                        FileCountsLogger.filesInDEV++;
                        Files.move(file.toPath(), Paths.get("src/main/resources/DEV/" + fileName), StandardCopyOption.REPLACE_EXISTING);
                    } else if (fileName.endsWith(".jar")) {
                        if (FileCreationDateAnalyzer.isFileCreationDateEven(file)) {
                            FileCountsLogger.filesInDEV++;
                            Files.move(file.toPath(), Paths.get("src/main/resources/DEV/" + fileName), StandardCopyOption.REPLACE_EXISTING);
                        } else {
                            FileCountsLogger.filesInTEST++;
                            Files.move(file.toPath(), Paths.get("src/main/resources/TEST/" + fileName), StandardCopyOption.REPLACE_EXISTING);
                        }
                    }
                    FileCountsLogger.updateCountsFile();
                }
            }
        }
    }

    private void initializeDirectiories() throws Exception {

        if (doAllOfRootFilesExist()) {
            return;
        }
        ResourcesFileCreator.createDirectory("HOME");
        ResourcesFileCreator.createDirectory("DEV");
        ResourcesFileCreator.createDirectory("TEST");
    }

    private boolean doAllOfRootFilesExist() {

        File home, dev, test;
        home = new File("src/main/resources/HOME");
        dev = new File("src/main/resources/DEV");
        test = new File("src/main/resources/TEST");

        this.homeDirectory = home;
        return (home.exists() && dev.exists() && test.exists());
    }
}
