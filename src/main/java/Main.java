import java.io.IOException;

public class Main {

    public static void main(String[] args) throws Exception {
        Runner runner = new Runner();
        runner.run();
    }
}