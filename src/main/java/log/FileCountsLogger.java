package log;

import analyzer.FileAnalyzer;
import creator.ResourcesFileCreator;

import java.io.File;
import java.io.PrintWriter;


public class FileCountsLogger {

    public static int processedFiles = 0;
    public static int filesInDEV = 0;
    public static int filesInTEST = 0;

    static File counts;

    public static void updateCountsFile() throws Exception {
        clearCountsFileContent();
        PrintWriter writer = new PrintWriter("src/main/resources/HOME/counts.txt");
        writer.write("Liczba przeprocesowanych plików: " + processedFiles + "\n");
        writer.write("Pliki umieszczone w katalogu DEV: " + filesInDEV + "\n");
        writer.write("Pliki umieszczone w katalogu TEST: " + filesInTEST + "\n");
        writer.close();
    }

    private static void clearCountsFileContent() throws Exception {
        PrintWriter writer = new PrintWriter("src/main/resources/HOME/counts.txt");
        writer.write("");
        writer.close();
    }

    static {
        if (FileAnalyzer.fileExistsInResources("HOME/counts.txt")) {
            counts = ResourcesFileCreator.createFile("HOME/counts.txt");
        }
    }
}
