package creator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.io.File;

public class ResourcesFileCreator {

    public static File createFile(String filename) {
        try {
            Files.createFile(Paths.get("src/main/resources/" + filename)).toFile();
        } catch (IOException exc) {
            System.out.println("Error while creating resouce file: " + filename);
        }
        return null;
    }

    public static File createDirectory(String directoryName) throws Exception {
        return Files.createDirectory(Paths.get("src/main/resources/" + directoryName)).toFile();
    }
}
